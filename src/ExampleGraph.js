import React, {useLayoutEffect} from 'react';
import * as d3 from 'd3';

const ExampleGraph = (props) => {
  const {pointerLocation, setPointerLocation, data} = props;

  const graph = React.createRef();

  useLayoutEffect(() => {
    const renderGraph = () => {

      function handleMouseMove(loc) {
        setPointerLocation(loc[0]);
      }

      const height = 100;
      const width = 500;


      const svg = d3.select(graph.current)
        .attr('height', height)
        .attr('width', width)

      let exampleGraph;
      let pointerCursor;
      let pointerTracker;

      if(svg.nodes()[0].children.length === 0){

        exampleGraph = svg
          .append('g')
          .attr('class','mainGraph')
          .selectAll('rect')
          .data(data);

        pointerCursor= svg
          .append('g')
          .attr('class','pointerCursor')
          .append('rect')

        pointerTracker= svg
          .append('g')
          .attr('class','cursorTracker')
          .append('rect')
          .attr("opacity","0")
          .attr('x', 0)
          .attr('y', 0)
          .attr('width',width) 
          .attr('height', height)
          .on('mousemove',(e) => handleMouseMove(d3.pointer(e)))


      } else {

        exampleGraph = svg
          .select('g.exampleGraph')
          .selectAll('rect')
          .data(data)
          .select('rect')

        pointerCursor= svg
          .select('g.pointerCursor')
          .select('rect')

        pointerTracker = svg
          .select('g.pointerTracker')
          .select('rect')
      }

      exampleGraph.exit().remove()


      exampleGraph
        .enter()
        .append('rect')
        .merge(exampleGraph)
        .attr('fill','blue')
        .attr('stroke-width',3)
        .attr('stroke','white')
        .attr('x', (d, i) => {
          let sum = 0;
          data.forEach((datapoint, index) => {
            if(index < i){
              sum = sum + datapoint.value*20;
            }
          })
          return sum
        })
        .attr('y', 25)
        .attr('width', (d,i) => d.value*20)
        .attr('height', 50)

      if(pointerLocation !== null){


        pointerCursor 
          .attr('fill','black')
          .attr('x', pointerLocation)
          .attr('y', 0)
          .attr('width',3) 
          .attr('height', height)
      }
    }

    if (graph.current && data){
      renderGraph();
    }
  }, [data, graph])

  return(
    <div>
      <h2>Graph title</h2>
      <svg ref={graph}/>
    </div>
  )
}
export default ExampleGraph;
