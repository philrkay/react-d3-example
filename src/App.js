import React, {useState} from 'react';
import './App.css';
import ExampleGraph from './ExampleGraph.js'

function App() {
  const [pointerLocation, setPointerLocation] = useState(null)
  const [data, setData] = useState([
    {id: 1, value: 2},
    {id: 2, value: 3},
    {id: 3, value: 5},
    {id: 4, value: 2}
  ])

  return (
    <div className="App">
      <div>
        <h1>GRAPH EXAMPLE</h1>
        <div className="graph">
          <span>React says cursor is at: {pointerLocation}</span>
          <ExampleGraph 
            pointerLocation={pointerLocation}
            setPointerLocation={(loc) => setPointerLocation(loc)}
            data={data}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
